import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiRestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiRestProvider {

	configUrl = 'http://iushumani.org/dsc_webservice/public/';

  constructor(public http: HttpClient) {
    //console.log('Hello ApiRestProvider Provider');
  }

  /*
    GET FUNCTIONS
  */
  getArticles() {
  	return this.http.get(this.configUrl+'articles');
  }

  getIssuesVol(){
    return this.http.get(this.configUrl+'allissues');
  }

/*
  SEARCH FUNCTION
*/

  searchIssues(endpoint: string, text: string, body: string){
     return this.http.post(this.configUrl+endpoint+'?search='+text, body);
  }
}
