import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
//import { Directive } from '@angular/core';

import {ApiRestProvider} from '../../providers/api-rest/api-rest';
import {TruncatePipe} from '../../pipes/truncate/truncate';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {
	
  pet: string = "news";
  newsData: any;
  volData: any; 
  searchData: any; 
  loadingSpiner = 'loading';
  textSearch: any;
  loadingsearch: any;
  constructor(
    public navCtrl: NavController,
    private api: ApiRestProvider,
    private document: DocumentViewer) {

  	this.showNews();
  }

  showNews(){
  	this.api.getArticles()
    .subscribe(data =>  {
       this.newsData = data['issues'];
       this.loadingSpiner ="stop"
       //console.log(this.newsData)
    });
  }

  showIssuesVol(){
  	this.api.getIssuesVol()
    .subscribe(data =>  {
       this.volData = data['issues'];
       //const articlesLength = this.volData.map(x => x.publisehd_articles);
       //console.log(articlesLength.length)
    });
  }

  viewPDF($title, $url){
    const options: DocumentViewerOptions = {
      title: $title
  }

    this.document.viewDocument($url, 'application/pdf', options)
  }

  search(ev: any){
    let val = ev.target.value;
    this.textSearch = ev.target.value;
    this.loadingsearch ="loading";

      if (val && val.trim() != '') {
        this.api.searchIssues('search_submission', val, 'isuhumani')
        .subscribe(data =>  {
           this.searchData = data['articles'];
           this.loadingsearch =null;
           console.log(this.searchData)
        });
      } else {

      }
  }

}
